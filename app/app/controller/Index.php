<?php

/**
 * Created By PhpStorm
 * User sclecon
 * Contact Email 27941662@qq.com
 * Time 2021/9/29 11:38
 */

namespace app\controller;

use wowo\instance\View;

class Index
{
    public function index(){
        return View::fetch('index', ['navtitle'=>'wowoDiscuz']);
    }
}