<?php

/**
 * Created By PhpStorm
 * User sclecon
 * Contact Email 27941662@qq.com
 * Time 2021/10/21 15:10
 */
 
return [
    // 判断用户是否登录 未登录则跳转至登录界面
    \app\middleware\Login::class
];