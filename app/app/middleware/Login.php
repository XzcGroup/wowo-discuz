<?php

/**
 * Created By PhpStorm
 * User sclecon
 * Contact Email 27941662@qq.com
 * Time 2021/10/22 15:56
 */


namespace app\middleware;


use wowo\instance\Config;
use wowo\instance\Discuz;

class Login
{
    public function handle($request, \Closure $next){
        if (Config::get('member') === false){
            Discuz::toLogin(wowoUrl('index', [], false));
        }
        return $next($request);
    }
}