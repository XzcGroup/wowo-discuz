<?php

/**
 * Created By PhpStorm
 * User sclecon
 * Contact Email 27941662@qq.com
 * Time 2021/8/26 10:45
 */

if (!defined('WOWODISCUZ')){
    exit('wowoDiscuz fail');
}

include __DIR__.'/vendor/autoload.php';
\wowo\instance\App::run();