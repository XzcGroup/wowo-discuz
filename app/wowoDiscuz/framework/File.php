<?php

/**
 * Created By PhpStorm
 * User sclecon
 * Contact Email 27941662@qq.com
 * Time 2021/8/26 11:18
 */


namespace wowo;


class File
{
    protected $includeFiles = [];

    public function getDirFiles($dir, $ext = 'php'){
        if (is_dir($dir) === false){
            return false;
        }
        $files = scandir($dir);
        foreach ($files as $key => $file){
            if (in_array($file, ['.', '..']) or count(explode('.', $file)) === 1 or end(explode('.', $file)) !== $ext){
                unset($files[$key]);
            }
        }
        return array_values($files);
    }

    public function includeFile(string $dir, $file = ''){
        $file = $file ? $dir.DIRECTORY_SEPARATOR.$file : $dir;
        if (file_exists($file) === false){
            return false;
        } else if (in_array($file, $this->includeFiles) === true){
            return true;
        }
        $this->includeFiles[] = $file;
        include_once $file;
        return true;
    }
}