<?php

/**
 * Created By PhpStorm
 * User sclecon
 * Contact Email 27941662@qq.com
 * Time 2021/10/21 10:45
 */


namespace wowo;

class Response
{
    public function http(){
        $object = \wowo\instance\Request::getObject();
        return is_object($object) ? $object() : \wowo\instance\Middleware::dispatch();
    }

    public function output($response){
        if (is_object($response)){
            $response->output();
        } else if (is_string($response)){
            $this->stringOutput($response);
        }
    }

    public function setHeader($headers){
        foreach ($headers as $field => $value){
            header(str_replace(['field', 'value'], [$field, $value], 'field: value'));
        }
    }

    protected function stringOutput($html){
        echo $html;exit;
    }

}