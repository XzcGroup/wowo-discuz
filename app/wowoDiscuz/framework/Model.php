<?php

/**
 * Created By PhpStorm
 * User sclecon
 * Contact Email 27941662@qq.com
 * Time 2021/9/15 14:42
 */


namespace wowo;


class Model extends Db
{
    /**
     * @var string 自增ID
     */
    protected $increment = 'id';

    /**
     * @var string 表名称
     */
    protected $tableName;

    /**
     * @var integer 时间戳 不定义则不进行自动更新
     */
    protected $timeStamp = false;

    public function __construct()
    {
        $this->table($this->getTableName());
        $this->setPk($this->increment);
    }

    private function getTableName(){
        $this->tableName = $this->tableName ?: toUnderScore(end(explode('\\', get_class($this))));
        return $this->tableName;
    }

}