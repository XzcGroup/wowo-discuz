<?php

/**
 * Created By PhpStorm
 * User sclecon
 * Contact Email 27941662@qq.com
 * Time 2021/10/22 11:05
 */


namespace wowo;


class View
{
    protected $response;
    protected $params = [];
    protected $header = [];
    protected $file;

    public function fetch($file, $params = [], $header = []){
        $this->response = $this->getTemplateFile($file);
        $this->params = array_merge($this->params, $params);
        $this->header = array_merge($this->header, $header);
        $this->file = $file;
        return $this;
    }

    public function output(){
        global $_G;
        foreach ($this->params as $field => $param){
            $$field = $param;
        }
        \wowo\instance\Response::setHeader($this->header);
        include $this->response;
    }

    protected function getTemplateFile($file){
        $filePath = app()->discuzPath.str_replace('/', app()->directory, 'source/plugin/'.app()->pluginName.'/app/app/view/'.$file.'.htm');
        if (file_exists($filePath) === false){
            throw new \Exception(str_replace("{file}", 'source/plugin/'.app()->pluginName.'/app/app/view/'.$file.'.htm', "模板文件”{file}“不存在"));
        }
        return template('diy:'.$file, 0, 'source/plugin/'.app()->pluginName.'/app/app/view');
    }
}