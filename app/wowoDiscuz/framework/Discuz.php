<?php

/**
 * Created By PhpStorm
 * User sclecon
 * Contact Email 27941662@qq.com
 * Time 2021/8/30 10:46
 */


namespace wowo;


class Discuz
{
    public $rootPath = '';

    protected $request;

    public function __construct(App $app, Request $request)
    {
        $this->rootPath = $app->discuzPath;
        $this->request = $request;
    }

    public function isInit(){
        return defined('IN_DISCUZ');
    }

    public function init(){
		if ($this->isInit() === false) {
            $discuz = $this->getDiscuz();
            $discuz->cachelist = $cachelist;
            $discuz->init();
            global $_G;
            list($port, $host) = explode('//', $_G['siteurl']);
            $siteurls = explode('/', $host);
            $_G['siteurl'] = $port.'//'.$siteurls[0].'/';
		}
    }

    public function toLogin($referer = false){
        global $_G;
        $loginUrl = (isset($_G['siteurl']) ? $_G['siteurl'] : '/').'member.php?mod=logging&action=login'.($referer ? '&referer='.urlencode($referer) : '');
        $this->request->toUrl($loginUrl);
    }

    protected function getDiscuz(){
        include $this->rootPath.'source/class/class_core.php';
        return \C::app();
    }
}