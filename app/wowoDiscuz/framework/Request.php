<?php

/**
 * Created By PhpStorm
 * User sclecon
 * Contact Email 27941662@qq.com
 * Time 2021/9/15 15:28
 */


namespace wowo;


class Request
{
    public $routeParams;

    public function __construct()
    {
        $this->initRequest();
        $this->checkObject();
        $this->checkAction();
    }

    /**
     * 获取get参数
     * @param string $field 参数字段
     * @param bool $default 参数默认值
     * @param string $function 过滤函数
     * @return array|bool|mixed
     */
    public function get($field = "", $default = false, $function = ''){
        $response = $_GET;
        if (is_string($field) and strlen($field)){
            $response = isset($_GET[$field]) ? $_GET[$field] : $default;
        }
        return ($function and (function_exists($function) or is_object($function))) ? $function($response) : $response;
    }

    public function post($field = "", $default = false, $function = ''){
        $response = $_POST;
        if (is_string($field) and strlen($field)){
            $response = isset($_POST[$field]) ? $_POST[$field] : $default;
        }
        return ($function and (function_exists($function) or is_object($function))) ? $function($response) : $response;
    }

    public function param($field = "", $default = false, $function = ''){
        $_params = array_merge($_POST, $_GET);
        $response = $_params;
        if (is_string($field) and strlen($field)){
            $response = isset($_params[$field]) ? $_params[$field] : $default;
        }
        return ($function and (function_exists($function) or is_object($function))) ? $function($response) : $response;
    }

    public function getObject(){
        return $this->routeParams['object'];
    }

    public function getAction(){
        return $this->routeParams['action'];
    }

    private function initRequest(){
        $rewriteRoot = reset(explode('?', reset(explode('/', substr($_SERVER['REQUEST_URI'], 1)))));
        if (\wowo\instance\Config::get('route.rewriteRoot')){
            if (\wowo\instance\Config::get('route.rewriteRoot') != $rewriteRoot){
                throw new \Exception('The Request-Uri RootPath Error');
            }
        }else{
            throw new \Exception('config the rewriteRoot is null');
        }
        $rewrite = reset(explode('?', substr($_SERVER['REQUEST_URI'], strlen($rewriteRoot)+2)));
        $routeParams = \wowo\instance\Route::getRequest($rewrite, $this->getMethod());
        $this->routeParams = $routeParams;
        $_GET = array_merge($_GET, $routeParams['_get']);
        unset($_GET['id']);
    }

    private function checkObject(){
        if (!$this->routeParams === null){
            throw new \Exception('Request Route Is Null');
        }
        if (is_string($this->routeParams['object'])){
            if (strlen($this->routeParams['object']) === 0){
                throw new \Exception('Request Route Is Config Error');
            } else if (class_exists($this->routeParams['object']) === false){
                throw new \Exception('Controller "'.$this->routeParams['object'].'" Not Found');
            }
        } else if (!is_object($this->routeParams['object'])) {
            throw new \Exception('Request Route Is Type Error');
        }
    }

    private function checkAction(){
        if (is_object($this->routeParams['object']) === false){
            if (in_array($this->routeParams['action'], get_class_methods($this->routeParams['object'])) === false){
                throw new \Exception('The '.$this->routeParams['action'].' method cannot be found in '.$this->routeParams['object']);
            }
        }
    }

    public function getMethod(){
        return $_SERVER['REQUEST_METHOD'];
    }

    public function toUrl($url){
        Header("Location: $url");
    }
}