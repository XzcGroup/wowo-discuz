<?php

/**
 * Created By PhpStorm
 * User sclecon
 * Contact Email 27941662@qq.com
 * Time 2021/8/26 15:19
 */


namespace wowo;


class Instance
{
    protected static function make(){
        return Container::get(static::getCLassName());
    }

    protected static function getCLassName(){
        return self::class;
    }

    protected static function invokeArgs(string $method, array $params = []){
        return Container::getInstance()->invokeArgs(static::getCLassName(), $method, $params);
    }

    public static function getInstance(){
        return self::make();
    }

    public static function __callStatic($name, $arguments)
    {
        return call_user_func_array([self::make(), $name], self::invokeArgs($name, $arguments));
    }
}