<?php

/**
 * Created By PhpStorm
 * User sclecon
 * Contact Email 27941662@qq.com
 * Time 2021/8/26 11:05
 */


namespace wowo;


class Config
{
    protected $config;
    protected $app;
    protected $file;

    public function __construct(App $app, File $file){
        $this->app = $app;
        $this->file = $file;
        $this->loadAllFrameWorkConfig();
        $this->loadDiscuzConfig();
        $this->loadPluginConfig();
        $this->loadMemberConfig();
        $this->appendAppConfig();
    }

    public function get($key = "", $default = ''){
        if (is_string($key) === false){
            return $default;
        } else if (strlen($key) === 0){
            return $this->config;
        }
        $keys = explode('.', $key);
        $config = $this->config;
        foreach ($keys as $key){
            if (array_key_exists($key, $config) === false){
                return $default;
            }
            $config = $config[$key];
        }
        return $config;
    }

    public function set($key, $data){
        if (is_string($key) === false or strlen($key) == 0){
            return false;
        }
        $keys = explode('.', $key);
        for ($i = count($keys)-1; $i >= 0; $i--){
            $data = [$keys[$i]=>$data];
        }
        $config = $this->setDataRule($data, $this->config);
        $this->config = $config;
        return $this->get();
    }

    public function load($key){
        return include (new App())->getRootUrl().'config'.DIRECTORY_SEPARATOR.$key.'.php';
    }

    protected function setDataRule($data, &$config){
        foreach ($data as $key => $datum){
            if (array_key_exists($key, $config)){
                if (is_array($datum) && is_array($config[$key])){
                    $config[$key] = $this->setDataRule($datum, $config[$key]);
                }else{
                    $config[$key] = $datum;
                }
            }else{
                $config[$key] = $datum;
            }
        }
        return $config;
    }

    protected function loadAllFrameWorkConfig(){
        $files = (new File())->getDirFiles((new App())->getRootUrl().'config');
        if (!$files){ return false; }
        foreach ($files as $file){
            $this->config[str_replace('.php', '', $file)] = $this->load(str_replace('.php', '', $file));
        }
    }

    protected function loadDiscuzConfig(){
        global $_G;
        return $this->set('discuz', $_G['config']);
    }

    protected function loadPluginConfig(){
        global $_G;
        loadcache('plugin');
        $config = $_G['cache']['plugin'][$this->app->getPluginName()] ?: [];
        foreach ($config as &$item){
            if (is_string($item) && is_array(unserialize($item))){
                $item = unserialize($item);
            }
        }
        return $this->set('plugin', $config);
    }

    protected function loadMemberConfig(){
        global $_G;
        $this->set("member", $_G['uid'] ? $_G['member'] : false);
    }

    protected function appendAppConfig(){
        global $_G;
        $this->set('app.siteUrl', $_G['siteurl'].$this->get('route.rewriteRoot'));
    }
}