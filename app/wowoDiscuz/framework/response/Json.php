<?php

/**
 * Created By PhpStorm
 * User sclecon
 * Contact Email 27941662@qq.com
 * Time 2021/10/22 14:14
 */


namespace wowo\response;


class Json
{
    protected $response;
    protected $header = ['content-type'=>'application/json'];

    public function fetch($data, $header = []){
        $this->response = $data;
        $this->header = array_merge($this->header, $header);
        return $this;
    }

    public function output(){
        \wowo\instance\Response::setHeader($this->header);
        echo $this->toJson();
    }

    private function toJson(){
        return json_encode($this->response);
    }
}