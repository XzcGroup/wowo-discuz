<?php

/**
 * Created By PhpStorm
 * User sclecon
 * Contact Email 27941662@qq.com
 * Time 2021/10/21 10:51
 */


namespace wowo\instance;


use wowo\Instance;

/**
 * Class Response
 * @package wowo\instance
 * @uses \wowo\Response
 * @method static mixed http() 获取请求结果
 * @method static bool output(mixed $response) 输出处理结果
 * @method static null setHeader(array $headers) 设置header头
 */
class Response extends Instance
{
    protected static function getCLassName()
    {
        return \wowo\Response::class;
    }
}