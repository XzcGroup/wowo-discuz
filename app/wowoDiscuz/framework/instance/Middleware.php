<?php

/**
 * Created By PhpStorm
 * User sclecon
 * Contact Email 27941662@qq.com
 * Time 2021/10/21 15:34
 */


namespace wowo\instance;


use wowo\Instance;

/**
 * Class Middleware
 * @package wowo\instance
 * @uses \wowo\Middleware
 * @method static mixed dispatch() 通过中间件做执行
 */
class Middleware extends Instance
{
    protected static function getCLassName()
    {
        return \wowo\Middleware::class;
    }
}