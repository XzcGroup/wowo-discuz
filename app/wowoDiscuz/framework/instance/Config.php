<?php

/**
 * Created By PhpStorm
 * User sclecon
 * Contact Email 27941662@qq.com
 * Time 2021/8/27 16:33
 */

namespace wowo\instance;

use wowo\Instance;

/**
 * Class Config
 * @uses \wowo\Config
 * @package wowo\instance
 * @method static mixed get(string $key = '', $default = false) 获取配置信息
 * @method static mixed set(string $key, $data) 修改设置配置信息
 */
class Config extends Instance
{
    protected static function getCLassName(){
        return 'wowo\Config';
    }
}