<?php

/**
 * Created By PhpStorm
 * User sclecon
 * Contact Email 27941662@qq.com
 * Time 2021/9/15 15:30
 */


namespace wowo\instance;


use wowo\Instance;

/**
 * Class Request
 * @package wowo\instance
 * @uses \wowo\Request
 * @method static mixed get(string $field = '', mixed $default = false, string|mixed $function = false) 获取get参数
 * @method static mixed post(string $field = '', mixed $default = false, string|mixed $function = false) 获取post参数
 * @method static mixed param(string $field = '', mixed $default = false, string|mixed $function = false) 获取get和post参数
 * @method static mixed getObject() 获取控制器路径或闭包函数
 * @method static mixed getAction() 获取控制器方法
 * @method static mixed toUrl(string $url) 跳转到网址
 */
class Request extends Instance
{
    protected static function getCLassName()
    {
        return \wowo\Request::class;
    }
}