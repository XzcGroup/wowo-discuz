<?php

/**
 * Created By PhpStorm
 * User sclecon
 * Contact Email 27941662@qq.com
 * Time 2021/8/30 10:59
 */


namespace wowo\instance;


use wowo\Instance;

/**
 * Class Discuz
 * @uses \wowo\Discuz
 * @package wowo\instance
 * @method static mixed init() 初始化Discuz系统
 * @method static string isInit() 判断是否已经初始化了Discuz系统
 * @method static mixed toLogin(bool|string $referer = false) 跳转至登录界面
 */
class Discuz extends Instance
{
    protected static function getCLassName()
    {
        return '\wowo\Discuz';
    }
}