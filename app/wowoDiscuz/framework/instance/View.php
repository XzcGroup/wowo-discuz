<?php

/**
 * Created By PhpStorm
 * User sclecon
 * Contact Email 27941662@qq.com
 * Time 2021/10/22 11:39
 */


namespace wowo\instance;


use wowo\Instance;

/**
 * Class View
 * @package wowo\instance
 * @uses \wowo\View
 * @method static array fetch(string $file, array $params = [], array $header = []) 返回HTML页面
 * @method static null output() 渲染HTML页面
 */
class View extends Instance
{
    protected static function getCLassName()
    {
        return \wowo\View::class;
    }
}