<?php

/**
 * Created By PhpStorm
 * User sclecon
 * Contact Email 27941662@qq.com
 * Time 2021/8/30 9:54
 */


namespace wowo\instance;
use wowo\Instance;

/**
 * Class App
 * @package wowo\instance
 * @uses \wowo\App
 * @method static mixed run() 初始化框架
 * @method static string getPluginName() 获取插件标识
 */
class App extends Instance
{
    protected static function getCLassName()
    {
        return 'wowo\App';
    }
}