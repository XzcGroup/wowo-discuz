<?php

/**
 * Created By PhpStorm
 * User sclecon
 * Contact Email 27941662@qq.com
 * Time 2021/9/29 11:25
 */


namespace wowo\instance;


use wowo\Instance;

/**
 * Class File
 * @package wowo\instance
 * @uses \wowo\File
 * @method static mixed getDirFiles(string $dir, string $ext = 'php') 获取目录文件
 * @method static bool includeFile(string $dir, string $file = '') 引入PHP文件
 */
class File extends Instance
{
    protected static function getCLassName()
    {
        return 'wowo\File';
    }
}