<?php

/**
 * Created By PhpStorm
 * User sclecon
 * Contact Email 27941662@qq.com
 * Time 2021/10/22 14:17
 */


namespace wowo\instance\response;


use wowo\Instance;

/**
 * Class Json
 * @package wowo\instance\response
 * @uses \wowo\response\Json
 * @method static \wowo\response\Json fetch($data, array $header = []) 返回json数据
 * @method static null output() 渲染json数据
 */
class Json extends Instance
{
    protected static function getCLassName()
    {
        return \wowo\response\Json::class;
    }
}