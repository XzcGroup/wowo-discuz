<?php

/**
 * Created By PhpStorm
 * User sclecon
 * Contact Email 27941662@qq.com
 * Time 2021/9/15 15:55
 */


namespace wowo\instance;


use wowo\Instance;

/**
 * Class Route
 * @package wowo\instance
 * @uses \wowo\Route
 * @method static array|mixed getRequest(string $rewrite = '', string $method = '') 通过请求链接获取对应路由或直接指向控制器
 * @method static mixed get(string $route, mixed $class, string $action = '') 设置GET请求路由
 * @method static mixed post(string $route, mixed $class, string $action = '') 设置POST请求路由
 * @method static string getUrl(string $path, array $params = []) 根据参数获取网址
 */
class Route extends Instance
{
    protected static function getClassName(){
        return \wowo\Route::class;
    }
}