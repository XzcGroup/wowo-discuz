<?php

/**
 * Created By PhpStorm
 * User sclecon
 * Contact Email 27941662@qq.com
 * Time 2021/8/26 10:09
 */
namespace wowo;

class App
{
    public $name = 'wowoDiscuz';
    public $version;
    public $wowoPath;
    public $appPath;
    public $directory;


    public function __construct()
    {
        $this->version = '0.1';
        $this->wowoPath = substr(__DIR__, 0, -9);
        $this->appPath = substr(__DIR__, 0, -20);
        $this->directory = DIRECTORY_SEPARATOR;
        $this->pluginName = end(explode(DIRECTORY_SEPARATOR, substr(__DIR__, 0, -25)));
        $this->discuzPath = implode($this->directory, array_slice(explode($this->directory, $this->appPath), 0, count(explode($this->directory, $this->appPath))-5)).$this->directory;
        $this->initFunction();
    }

    public function run(){
        // 初始化Discuz
        \wowo\instance\Discuz::init();

        // 初始化请求
        \wowo\instance\Response::output(\wowo\instance\Response::http());
    }

    public function getRootUrl(){
        return substr(__DIR__, 0, -20);
    }

    public function getPluginName(){
        return $this->pluginName;
    }

    protected function initFunction(){
        \wowo\instance\File::includeFile($this->wowoPath.'src'.$this->directory, 'functions_include.php');
    }
}