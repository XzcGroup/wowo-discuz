<?php

/**
 * Created By PhpStorm
 * User sclecon
 * Contact Email 27941662@qq.com
 * Time 2021/8/26 11:35
 */

if (function_exists('wowohalt') === false) {
    function wowohalt($value, $exit = true){
        echo "<pre style='padding:20px;background: #1d333e;color: #dcdcdc;margin: 10px;'>";
        var_dump($value);
        echo "</pre>";
        if ($exit)
            exit;
    }
}

if (function_exists('app') === false) {
    function app(){
        return \wowo\instance\App::getInstance();
    }
}

if (function_exists('file') === false) {
    function file(){
        return new \wowo\File();
    }
}

if (function_exists('config') === false) {
    function config($key = '', $default = false){
        if ($key && is_string($key)){
            return \wowo\instance\Config::get($key, $default);
        }
        return \wowo\instance\Config::getInstance();
    }
}

//驼峰命名转下划线命名
if (function_exists('toUnderScore') === false){
    function toUnderScore($str)
    {
        $dstr = preg_replace_callback('/([A-Z]+)/',function($matchs) {return '_'.strtolower($matchs[0]);},$str);
        return trim(preg_replace('/_{2,}/','_',$dstr),'_');
    }
}


//下划线命名到驼峰命名
if (function_exists('toCamelCase') === false){
    function toCamelCase($str)
    {
        $array = explode('_', $str);
        $result = $array[0];
        $len=count($array);
        if($len>1)
        {
            for($i=1;$i<$len;$i++)
            {
                $result.= ucfirst($array[$i]);
            }
        }
        return $result;
    }
}

//通过二维数组中某个字段的值获取当前的父级元素
if (function_exists('filterByValue') === false){
    function filterByValue($array, $index, $value){
        $newArray = false;
        if(is_array($array) && count($array)>0) {
            foreach(array_keys($array) as $key){
                $temp[$key] = $array[$key][$index];
                if ($temp[$key] == $value or in_array($temp[$key], (array)$value)){
                    $newArray[] = $array[$key];
                }
            }
        }
        return $newArray;
    }
}

if (function_exists('wowoUrl') === false){
    /**
     * 快捷获取路由网址
     * @param string $path 控制器和方法字符串
     * @param array $params 网址参数 默认为空数组
     * @param bool $echo 是否直接输出
     * @return string 组合出来的URL网址
     */
    function wowoUrl($path, $params = [], $echo = true){
        if ($echo){
            echo \wowo\instance\Route::getUrl($path, $params);
        }else{
           return \wowo\instance\Route::getUrl($path, $params);
        }
    }
}

if (function_exists('wowoMsg') === false){
    /**
     * 请求终止 显示提示信息
     * @param string $msg 消息内容文本
     * @param string $url 跳转网址
     * @param bool $error 是否为错误消息
     */
    function wowoMsg($msg, $url = '', $error = true){
        $alert = $error ? 'error' : 'right';
        showmessage($msg, $url, [], [], ['alert'=>$alert]);
    }
}
